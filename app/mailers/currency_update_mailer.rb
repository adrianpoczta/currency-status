class CurrencyUpdateMailer < ActionMailer::Base

  def send_update_notification(user)
  	@user = user
    mail(to: user.email, subject: "Currencies was just updated - chceck our website")
  end

end