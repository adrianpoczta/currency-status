class MoneyController < ApplicationController
  def index
    @exchanges = Exchange.order("created_at DESC").page(params[:page]).per_page(10)
  end

  def show
    @currency = Currency.where(exchange_id: (params[:id]))
  end

  def refresh_rates
    exchange_time = Time.now - 8.hour - 15.minutes

    if Exchange.last.present?
      if Exchange.last.created_at.strftime("%y%m%d") == Time.now.strftime("%y%m%d")
        flash[:notice] = 'Everything is up to date.'
      else
        Exchange.save_xml_data(exchange_time)
      end
    else
      Exchange.save_xml_data(exchange_time)
      flash[:notice] = 'Results were updated.'
    end

    redirect_to money_index_path
  end
end