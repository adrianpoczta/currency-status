class ReportsController < ApplicationController
  def show
    @currency = Currency.where(code: (params[:id])).order("id DESC").page(params[:page]).per_page(10)
  end
end
