class BackgroundPricesUpdateWordker
  include Sidekiq::Worker

  sidekiq_options :failures => true

  def perform
    self.get_nbp_xml(Time.now)
    self.save_xml_data(Time.now)
  end
end

def get_nbp_xml(date)
  xml = Nokogiri::XML(open("http://www.nbp.pl/kursy/xml/LastC.xml"))
  xml.search('pozycja').map do |position|
    %w[nazwa_waluty przelicznik kod_waluty kurs_kupna kurs_sprzedazy].each_with_object({}) do |n, o|
      o[n] = position.at(n).text
    end
  end
end

def save_xml_data(date)
  currency_hash = get_nbp_xml(date)

  if currency_hash.present?
    exchange = Exchange.create(name: date.strftime("%d-%m-%Y"))
    currency_hash.each do |currency| 
      Currency.create(name: currency['nazwa_waluty'], converter: currency['przelicznik'], code: currency['kod_waluty'], 
                      buy_price: currency['kurs_kupna'].to_s.gsub(',', '.'), sell_price: currency['kurs_sprzedazy'].to_s.gsub(',', '.'),
                      exchange_id: exchange.id)
    end
  end
end