module ReportsHelper
  def average(*args)
    args.inject(:+).to_f / args.size
  end
end