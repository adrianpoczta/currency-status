Rails.application.routes.draw do
  
  devise_for :users
  root :to => "money#index"

  resources :users, except: [:show, :index]
  resources :reports
  resources :money do
    collection do
      get :refresh_rates
    end
  end

end
